# macchanger
You can change your MAC-address using the following code. use your network interface as <code>interface</code>. You can find it's name by using <code>ifconfig</code>.

    ifconfig <interface> down
    macchanger -m 00:d0:70:00:20:69 <interface>
    ifconfig <interface> up
    macchanger -s <interface>