# aircrack-ng

## set interface to monitore mode

You can set the interface using these commands:

    airmon-ng check kill
    airmon-ng start

## deauth devices

    aireplay-ng -0 <amount_of_deauths> -a <mac_of_AP> -c <mac_of_client> <interface>