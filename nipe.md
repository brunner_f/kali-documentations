# nipe

## installation

You can install nipe using the following commands:

    git clone https://github.com/htrgouvea/nipe && cd nipe
    sudo cpan install Try::Tiny Config::Simple JSON
    sudo perl nipe.pl install

## usage

You have the following options to use nipe:

    perl nipe.pl install
    perl nipe.pl start
    perl nipe.pl stop
    perl nipe.pl restart
    perl nipe.pl status